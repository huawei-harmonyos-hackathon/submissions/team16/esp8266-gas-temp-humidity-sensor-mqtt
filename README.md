# ESP8266 Gas Temp Humidity Sensor MQTT
# Hardware
Hardware consist of:
1. Wemos SHT30 shield https://www.amazon.com/Shield-Digital-Temperature-Humidity-Sensor/dp/B07V6VKDPH 
2. WEMOS D1 pro mini https://www.amazon.com/1PCS-WEMOS-mini-Pro-connector/dp/B06Y4YL54S
Modules are put together with goldpins. You can attach external antenna for better wifi coverage.
# Installation
To use this project you need Arduino IDE. You can get it here: https://www.arduino.cc/en/software
## Arduino libraries
### PubSub Client
You need library: PubSub Client from Nick O'Leary. Tested with version 2.8.0
https://pubsubclient.knolleary.net/ 
## Arduino boards
You need to add to preferences new link:
https://arduino.esp8266.com/stable/package_esp8266com_index.json
File -> Preferences -> Additional URL for boards manager
## Passwords
In file passwords.h.example there is example of passwords you need. Names are self explainable.
# Setup
## Open INO File
Open file wemos-temp-humidity.ino in Arduino IDE
## Edit paswords.h
Edit your settings in passwords.h
## Select board
In boards manager select: Generic ESP8266 Module
## Upload sketch
Upload sketch to board
