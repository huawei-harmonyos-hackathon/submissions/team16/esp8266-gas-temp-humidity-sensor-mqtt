#include <WEMOS_SHT3X.h>
#include "passwords.h"
 
SHT3X sht30(0x45);
 
//void setup() 
//{
//  Serial.begin(9600);
//}
// 
//void loop() {
// 
//  if(sht30.get()==0){
//    Serial.print("Temperature in Celsius : ");
//    Serial.println(sht30.cTemp);
//    Serial.print("Temperature in Fahrenheit : ");
//    Serial.println(sht30.fTemp);
//    Serial.print("Relative Humidity : ");
//    Serial.println(sht30.humidity);
//    Serial.println();
//  }
//  else
//  {
//    Serial.println("Error!");
//  }
//  delay(1000);
// 
//}

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define wifi_ssid WIFI_SSID
#define wifi_password WIFI_PASSWORD

#define mqtt_server MQTT_SERVER
#define mqtt_user MQTT_USER
#define mqtt_password MQTT_PASSWORD

#define humidity_topic "sensor/humidity"
#define temperature_topic "sensor/temperature"

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    // If you do not want to use a username and password, change next line to
    // if (client.connect("ESP8266Client")) {
    if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

bool checkBound(float newValue, float prevValue, float maxDiff) {
  return !isnan(newValue) &&
         (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff);
}

long lastMsg = 0;
float temp = 0.0;
float hum = 0.0;
float diff = 1.0;

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
//  Serial.println("after loop");

  long now = millis();
  if (now - lastMsg > 1000 && sht30.get()==0) {
    Serial.println("time to message");
    lastMsg = now;
    float newTemp = sht30.cTemp;
    float newHum = sht30.humidity;

    if (checkBound(newTemp, temp, diff)) {
      temp = newTemp;
      Serial.print("New temperature:");
      Serial.println(String(temp).c_str());
      client.publish(temperature_topic, String(temp).c_str(), true);
    }

    if (checkBound(newHum, hum, diff)) {
      hum = newHum;
      Serial.print("New humidity:");
      Serial.println(String(hum).c_str());
      client.publish(humidity_topic, String(hum).c_str(), true);
    }
  }
}
